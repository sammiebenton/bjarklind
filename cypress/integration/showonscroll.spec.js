/// <reference types="cypress" />

describe('should fade on scroll', () => {
  beforeEach(() => {
    cy.visit('http://localhost:3000/')
  })

  it('it should be on the page - element with the id name', () => {
    cy.get('#name')
  })
  
  it('should be invisible on load - have invisible class', () => {
    cy.get('#name').should('have.class', 'invisible')
  });

  it('should appear on scroll over 130 - have visible class', () => {
    cy.scrollTo(0, 131)
    cy.get('#name').should('have.class', 'visible')
  });
    
  it('should disappear on scroll under 130 - set invisible', () => {
    cy.scrollTo(0, 0)
    cy.get('#name').should('have.class', 'invisible')
  });
  
})
