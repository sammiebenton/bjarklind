/// <reference types="cypress" />

describe('contact form submit test', () => {
  beforeEach(() => {
    cy.visit('http://localhost:3000/contact')
  })

  it('should render a form', () => {
    cy.get('#contactForm')
  })
  it('show have these attributes', () => {
    cy.get('#remember_me')
    cy.get('input').should('have.attr', 'type')
    cy.get('input').should('have.attr', 'name')
  })
})