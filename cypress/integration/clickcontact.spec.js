/// <reference types="cypress" />
describe('click and alert', () => {
  it('should connect', () => {
    cy.visit('https://www.google.com')
  })

  it('should click accept', () => {
    cy.get('#L2AGLb').click()
  })
  it('search for Viktor', () => {
    cy.get('.gLFyf').type('Viktor')
    cy.get('.FPdoLc').click()
    cy.screenshot()
  })

  // it('click a project link', () => {
  //   cy.get('#Projects').click()
  // })

  // it('should alert on click', () => {
  //   cy.get('#contact-button').click()
  // })
})