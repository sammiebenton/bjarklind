import React, {MouseEvent, useEffect, useState} from 'react';

import {
  BrowserRouter as Router,
  Route,
  Switch,
  useLocation,
} from 'react-router-dom';

import TopNavbar from './layout/TopNavbar';

import links from './data/links.data.json'
import { Heading } from './layout/Heading';
import HomePage from './pages/HomePage';
import AboutPage from './pages/AboutPage';
import { ContactForm } from './components/ContactForm';
import ProjectPage from './pages/ProjectPage';
import DashboardPage from './pages/DashboardPage';

function App() {
  const [visible, setVisible] = useState(true)

  useEffect(() => {
    
  }, []);

  const handleClick = (e: MouseEvent<HTMLDivElement>) => {
    alert('clicked')
  }
  
  return (
    <div className="w-window flex flex-col h-9xl text-gray-700 mt-0 from-white to-indigo-600 pt-8">
      <h1 className="font-black absolute top-10 left-10">{process.env.FIRST_NAME}</h1>
      <div className="h-10 w-10 rounded-full bg-black fixed right-5 bottom-5 cursor-pointer z-50" id="contact-button" onClick={handleClick}>
        {/* <div className="bg-nav-image object-contain"></div> */}
      </div>

      <Router>
      <TopNavbar data={links} />
        <Switch>
          <Route exact path="/"><HomePage /></Route>
          <Route exact path="/about"><AboutPage /></Route>
          <Route exact path="/projects"><ProjectPage /></Route>
          <Route exact path="/maggi"><DashboardPage /></Route>
          <Route exact path="/contact" component={ContactForm} />
        </Switch>
      </Router>
      
    </div>
  );
}

export default App;
