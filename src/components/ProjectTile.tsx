import { useState, FC } from "react"

type Project = {
  id: number;
  title: string;
  description: string;
  image: string;
  link: string;
  pictures: any[]
}

interface ProjectTileProps {
  data: Project[]
}

export const ProjectTile: FC<ProjectTileProps> = ({ data }) => {
  
  return (
    <div className="container grid grid-flow-row sm:grid-cols-1 md:grid-cols-2 lg:grid-cols-3 xl:grid-cols-4 gap-6 shadow">
      {
        data.map(project => {
          return (
            <article className="shadow-lg rounded-sm min-h-15 sm:h-80 xl:h-48 w-auto p-4 bg-white relative overflow-hidden bg-black my-5" key={project.id}>
              <img alt="" src={project.image} className="absolute left-0 top-0 h-40 w-full h-full border-none z-0 bg-transparent"/>
              <div className="w-full z-30 just">
                  <p className="text-gray-800 text-lg font-medium mb-2">{project.title}</p>
                  <p className="text-gray-400 text-lg">
                      {project.description}
                  </p>
                  <button className="text-indigo-500 text-xl font-medium cursor-pointer">
                      more
                  </button>
              </div>
            </article>
          )
        })
      }
    </div>
  )
}