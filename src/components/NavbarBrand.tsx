import { useState } from "react"

export const NavbarBrand = () => {
  const [visibility, setVisibility] = useState('invisible')

  const isVisible = `p-4 top-0 left-2 fixed font-permanent-marker text-xl ${visibility}`
  
  window.addEventListener('scroll', () => {
    if(window.pageYOffset > 130){
      setVisibility('visible')
    }
    if(window.pageYOffset < 130){
      setVisibility('invisible')
    }
  })

  return <h5 className={isVisible} id="name">Magnús</h5>
  
}