import { FC, useEffect, useState } from "react"
import {
  useLocation
} from 'react-router-dom'

interface HeadingProps {
  title: string
}

export const Heading: FC<HeadingProps> = ({title}) => {
  const [fontSize, setFontSize] = useState('text-7xl')
  const [width, setWidth] = useState(window.innerWidth)
  const location = useLocation().pathname;
  console.log(location)


  

  return (
    <div className="flex justify-center mt-10">
      <h1 className={`font-monospace ${fontSize}`}>{title}</h1>
    </div>
  )
}