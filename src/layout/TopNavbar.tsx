import React, { FC, useState } from "react"
import { Link } from "react-router-dom"

type NavbarLink = {
  id: number,
  name: string,
  link: string,
  dropdown: any[],
  icon: string
}

interface TopNavbarProps {
  data: NavbarLink[]
}

const TopNavbar: FC<TopNavbarProps> = ({data}) => {
  const [visible, setVisible] = useState<boolean>(false)
  
  return (
    <nav className="max-h-12 w-screen mt-15 pt-5 pb-10 list-none flex justify-between align-center sticky top-0 z-50 bg-white" id="top_nav">
      <ul className="list-none flex w-screen justify-center">
        {
          data.map((link) => {
            if(link.dropdown.length > 0) {
              return <li key={link.id} className={`px-2 font-red-hat font-black font-bold visible`}><a href={link.link}>{link.name.toUpperCase()}</a></li>
            }else {
              return <Link key={link.id} to={link.link} className="px-2 font-red-hat font-bold visible" id={link.name}>{link.name.toUpperCase()}</Link>
            }
          })
        }
      </ul>

    </nav>
  )
}

export default TopNavbar
