import { Heading } from '../layout/Heading'
const AboutPage = () => {
  return (
    <div id="homepage">
      <div id="project-page" className="container center m-auto grid grid-cols-2 gap-4 flex flex-wrap flex-end">
      <div className="col pt-28">
      <p className="white font-red-hat text-2xl">Magnús Bjarklind er skrúðgarðyrkjumeistari og garðyrkjutæknir með yfir 14 ára starfsreynslu í ráðgjöf og þjónustu á sviði landslags- og garðyrkjutækni. Samhliða vinnu á verkfræðistofum hefur Magnús rekið eigið fyrirtæki frá árinu 2008. Magnús hefur, m.a. kennt við Landbúnaðarháskóla Íslands og var námsbrautarstjóri skrúðgarðyrkju við sömu stofnun. Magnús hefur haldið fjölda námskeiða og erinda á sviði garðyrkju, bæði hérlendis og erlendis. Hann hefur tekið þátt í fjölmörgum rannsóknum, skrifað greinar og gefið út bækur sem fjalla um garðyrkju og tengdar greinar.</p>
      </div>
    </div>
    </div>
  )
}
export default AboutPage