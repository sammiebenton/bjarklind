import { ProjectTile } from '../components/ProjectTile'
import projects from '../data/projects.data.json'


const ProjectPage = () => {
  return (
    <div className="container fluid center m-auto grid sm:grid-cols-1 md:grid-cols-3 lg:grid-cols-2 xl:grid-cols-2 gap-0 flex flex-wrap flex-end overflow-x-hidden pt-10">
      <div className="container flex justify-end align-center">
        <p className="col container white font-red-hat text-2xl p-4">Magnús Bjarklind er skrúðgarðyrkjumeistari og garðyrkjutæknir með yfir 14 ára starfsreynslu í ráðgjöf og þjónustu á sviði landslags- og garðyrkjutækni. Samhliða vinnu á verkfræðistofum hefur Magnús rekið eigið fyrirtæki frá árinu 2008. Magnús hefur, m.a. kennt við Landbúnaðarháskóla Íslands og var námsbrautarstjóri skrúðgarðyrkju við sömu stofnun. Magnús hefur haldið fjölda námskeiða og erinda á sviði garðyrkju, bæði hérlendis og erlendis. Hann hefur tekið þátt í fjölmörgum rannsóknum, skrifað greinar og gefið út bækur sem fjalla um garðyrkju og tengdar greinar.</p>
        <div className="col pt-28"></div>
      </div>
      <div className="col h-full w-full p-14 flex justify-center align-center">
        <video className="bg-black h-full w-full" src=""></video>
      </div>
      <div className="w-screen">
        <ProjectTile data={projects} />
      </div>
    </div>
  )
}
export default ProjectPage