import { Heading } from '../layout/Heading'
const HomePage = () => {
  return (
    <div className="" id="homepage">
      <Heading title={'Bjarklind & Kemp'.toUpperCase()} />
    </div>
  )
}
export default HomePage