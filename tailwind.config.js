module.exports = {
  purge: [
    './src/**/*.html',
    './src/**/*.js',
    './src/**/*.jsx',
    './src/**/*.ts',
    './src/**/*.tsx',
    './public/index.html',
  ],  
  darkMode: false,
  theme: {
    extend: {
      backgroundImage: {
        'nav-image': '/src/assets/images/maggiragna.png',
       },
       fontFamily: {
        'permanent-marker': ['"Permanent Marker"', 'cursive'],
        'red-hat': ['"Red Hat Display"', 'cursive']
      },
      Animation: {
        'fade-in-left': ['animate__animated animate__fadeinleft 2s'],
        'fade-out-left': ['animate__animated animate__fadeinleft 2s'],
        'fade-out-start': ['animate__animated animate__fadeinleft 0s']
      }
    }
  },
  variants: {},
  plugins: []
}
